module gitlab.com/gitlab-org/gitlab-pages

go 1.13

require (
	github.com/andybalholm/brotli v1.0.1
	github.com/cenkalti/backoff/v4 v4.0.2
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/golang/mock v1.3.1
	github.com/golangci/golangci-lint v1.27.0
	github.com/gorilla/context v1.1.1
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/securecookie v1.1.1
	github.com/gorilla/sessions v1.2.0
	github.com/hashicorp/go-multierror v1.1.1
	github.com/jstemmer/go-junit-report v0.9.1
	github.com/kardianos/osext v0.0.0-20190222173326-2bc1f35cddc0
	github.com/karlseguin/ccache/v2 v2.0.6
	github.com/karrick/godirwalk v1.10.12
	github.com/namsral/flag v1.7.4-pre
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/pires/go-proxyproto v0.2.0
	github.com/prometheus/client_golang v1.6.0
	github.com/rs/cors v1.7.0
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/stretchr/testify v1.6.1
	github.com/tj/assert v0.0.3 // indirect
	github.com/tj/go-redirects v0.0.0-20180508180010-5c02ead0bbc5
	gitlab.com/gitlab-org/go-mimedb v1.45.0
	gitlab.com/gitlab-org/labkit v1.3.0
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	golang.org/x/net v0.0.0-20201202161906-c7110b5ffcbb
	golang.org/x/sys v0.0.0-20210119212857-b64e53b001e4
	golang.org/x/tools v0.1.0 // indirect
)
